package com.indra.recruitment.Challenger.JPA.model;

public interface GenericModel<T> {

    /**
    * Gets the ID of the model.
    * 
    * @return ID of the model.
    */
   public abstract T getId(); 
   
   /**
    * Sets the ID of the model.
    * 
    * @param id
    * 		ID of the model.
    */
   public abstract void setId(T id);
}