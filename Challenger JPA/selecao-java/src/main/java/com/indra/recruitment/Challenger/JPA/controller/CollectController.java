package com.indra.recruitment.Challenger.JPA.controller;

import java.net.URI;

import javax.validation.Valid;

import com.indra.recruitment.Challenger.JPA.model.Collect;
import com.indra.recruitment.Challenger.JPA.service.CollectServiceImpl;
import com.indra.recruitment.Challenger.JPA.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@RestController
@RequestMapping("/user/collections")
@Api(tags = "Collect-Controller")
@NoArgsConstructor
@AllArgsConstructor
public class CollectController {

	@Autowired
	private CollectServiceImpl collectService;

	@GetMapping
	public Page<Collect> findAll(Pageable pageable) {
		return this.collectService.getRepository().findAll(pageable);
	}

	@PostMapping
	public ResponseEntity<Collect> create(@Valid @RequestBody Collect collect) {
		this.collectService.create(collect);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{collectId}")
				.buildAndExpand(collect.getId()).toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping("/{collectId}")
	public Collect findById(@PathVariable Long collectId) {
		return collectService.findById(collectId);
	}

	@PutMapping("/{collectId}")
	public ResponseEntity<Collect> update(@PathVariable long collectId, @Valid @RequestBody Collect collect) {
		collectService.update(collectId, collect);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{collectId}")
				.buildAndExpand(collect.getId()).toUri();
		return ResponseEntity.created(location).build();
	}

	@DeleteMapping("/{collectId}")
	public void deleteById(@PathVariable long collectId) {
		collectService.deleteById(collectId);
	}

	@GetMapping("/avgPurchasePriceByCountyName")
	public Double findAvgPurchasePriceCountyName(@RequestBody String countyName) {
		return this.collectService.getAveragePurchasePriceBasedOnCountyName(countyName);
	}

	@GetMapping("/avgSalePriceByCountyName")
	public Double findAvgSalePriceCountyName(@RequestBody String countyName) {
		return this.collectService.getAverageSalePriceBasedOnCountyName(countyName);
	}

	@GetMapping("/avgPurchaseAndSalePriceCollectsByCountyName")
	public Double findAvgPurchaseAndSalePriceCollectsByCountyName(@RequestBody String countyName) {
		Double averageSalePrice = this.findAvgSalePriceCountyName(countyName);
		Double averagePurchasePrice = this.findAvgPurchasePriceCountyName(countyName);
		return averagePurchasePrice + averageSalePrice / 2;
	}

	@GetMapping("/avgPurchaseAndSalePriceCollectsByBannerName")
	public Double findAvgPurchaseAndSalePriceCollectsByBannerName(@RequestBody String bannerName) {
		Double averageSalePrice = this.findAvgSalePriceBannerName(bannerName);
		Double averagePurchasePrice = this.findAvgPurchasePriceBannerName(bannerName);
		return averagePurchasePrice + averageSalePrice / 2;
	}

	@GetMapping("/avgPurchasePriceByBannerName")
	public Double findAvgPurchasePriceBannerName(String bannerName) {
		return this.collectService.getAveragePurchasePriceBasedOnBannerName(bannerName);
	}

	@GetMapping("/avgSalePriceByBannerName")
	public Double findAvgSalePriceBannerName(String bannerName) {
		return this.collectService.getAverageSalePriceBasedOnBannerName(bannerName);
	}

	@GetMapping("/collectsByRegionName")
	public Page<Collect> findCollectsByRegionName(@RequestBody String regionName, Pageable pageable) {
		return this.collectService.getCollectsByRegionName(regionName, pageable);
	}

	@GetMapping("/collectsByResellerName")
	public Page<Collect> findCollectsByResellerName(@RequestBody String resellerName, Pageable pageable) {
		return this.collectService.getRepository().findCollectsByResellerName(resellerName, pageable);
	}

	@GetMapping("collectsByCollectionDate")
	public Page<Collect> findCollectsByCollectionDate(@RequestBody String collectionDate, Pageable pageable) {
		return this.collectService
				.getRepository()
				.findByCollectionDate(DateUtil.getLocalDate(collectionDate),
				pageable);
	}
}
