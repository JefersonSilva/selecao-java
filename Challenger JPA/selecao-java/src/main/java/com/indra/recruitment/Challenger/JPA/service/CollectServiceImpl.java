package com.indra.recruitment.Challenger.JPA.service;

import com.indra.recruitment.Challenger.JPA.model.Collect;
import com.indra.recruitment.Challenger.JPA.repository.CollectRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CollectServiceImpl extends GenericService<Collect, CollectRepository> {
    
    // Average Purchase Price Based On County Name
    public Double getAveragePurchasePriceBasedOnCountyName(String countyName) {
        return getRepository().findAverageFuelPurchasePriceBasedOnCountyName(countyName);

    }

    // Average Sale Price Based On County Name
    public Double getAverageSalePriceBasedOnCountyName(String countyName) {
        return getRepository().findAverageFuelSalePriceBasedOnCountyName(countyName);
    }

    // Average Sale Price Based On Banner Name
    public Double getAverageSalePriceBasedOnBannerName(String bannerName) {
        return getRepository().findAverageFuelSalePriceBasedOnBannerName(bannerName);
    }

    // Average Purchase Price Based On Banner Name
    public Double getAveragePurchasePriceBasedOnBannerName(String bannerName) {
        return getRepository().findAverageFuelPurchasePriceBasedOnBannerName(bannerName);
    }

    // Collects By Region Name
    public Page<Collect> getCollectsByRegionName(String regionName, Pageable pageable) {
        return getRepository().findCollectsByRegionName(regionName, pageable);
    }
}
