package com.indra.recruitment.Challenger.JPA.controller;

import io.swagger.annotations.Api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.indra.recruitment.Challenger.JPA.model.User;
import com.indra.recruitment.Challenger.JPA.service.UserService;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/users")
@Api(tags = "User-Controller")
public class UserController {

	@Autowired
    private UserService userService;

    @GetMapping
    public Page<User> findAll(Pageable pageable) {
        return userService.getRepository().findAll(pageable);
    }

    @PostMapping
    public ResponseEntity<User> create(@Valid @RequestBody User user) {
        User u = userService.create(user);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{userId}").buildAndExpand(u.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{userId}")
    public User findById(@PathVariable long userId) {
        return userService.findById(userId);
    }

    @PutMapping("/{userId}")
    public ResponseEntity<User> update(@PathVariable long userId, @Valid @RequestBody User user) {
        User u =  userService.update(userId, user);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{userId}").buildAndExpand(u.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/{userId}")
    public void deleteById(@PathVariable long userId) {
        userService.getRepository().deleteById(userId);
    }

}
