package com.indra.recruitment.Challenger.JPA.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Collect implements GenericModel<Long> {

    public static final String PK = "collect_id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PK)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = Region.PK)
    @NotNull(message = "Region cannot be null")
    private Region region;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = State.PK)
    @NotNull(message = "State cannot be null")
    private State state;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = County.PK)
    @NotNull(message = "County cannot be null")
    private County county;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = Reseller.PK)
    @NotNull(message= "Reseller cannot be null")
    private Reseller reseller;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = Product.PK)
    @NotNull(message = "Product cannot be null")
    private Product product; //ok
    
    private Date collectionDate;

    private Double purchasePrice;

    private Double salePrice;

    private String measurementUnit;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = Banner.PK)
    private Banner banner;

}
