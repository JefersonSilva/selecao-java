package com.indra.recruitment.Challenger.JPA.controller;

import java.net.URI;

import com.indra.recruitment.Challenger.JPA.model.Collect;
import com.indra.recruitment.Challenger.JPA.service.ImportColletionsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter @Setter
@RestController
@RequestMapping("/user/collections")
public class ImportCollectionsController {

    @Autowired
    private ImportColletionsService service;

    @ApiOperation(value = "importCsv")
	@PostMapping(value = "/import")
	public ResponseEntity<Page<Collect>> importCsv(@RequestBody MultipartFile file) {
		service.importCsv(file);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(location).build();
	}

}