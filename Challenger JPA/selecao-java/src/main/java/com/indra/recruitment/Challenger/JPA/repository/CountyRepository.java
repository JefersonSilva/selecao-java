package com.indra.recruitment.Challenger.JPA.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.indra.recruitment.Challenger.JPA.model.County;

@Repository
public interface CountyRepository extends PagingAndSortingRepository<County, Integer> {
	
	Optional<County> findByName(String name);
	
	Optional<County> findById(String name);
}
