import { Banner } from './banner';
import { Region } from './region';
import { State } from './state';
import { County } from './county';
import { Product } from './product';
import { Reseller } from './reseller';

export class Collect {
    private _id: number;
    private _region: Region;
    private _state: State;
    private _county: County;
    private _product: Product;
    private _reseller: Reseller;
    private _collectionDate: Date;
    private _purchasePrice: number;
    private _salePrice: number;
    private _measurementUnit: string;
    private _banner: Banner;

    
    public get banner(): Banner {
        return this._banner;
    }
    public set banner(value: Banner) {
        this._banner = value;
    }
    public get measurementUnit(): string {
        return this._measurementUnit;
    }
    public set measurementUnit(value: string) {
        this._measurementUnit = value;
    }
    public get salePrice(): number {
        return this._salePrice;
    }
    public set salePrice(value: number) {
        this._salePrice = value;
    }
    public get purchasePrice(): number {
        return this._purchasePrice;
    }
    public set purchasePrice(value: number) {
        this._purchasePrice = value;
    }
    public get collectionDate(): Date {
        return this._collectionDate;
    }
    public set collectionDate(value: Date) {
        this._collectionDate = value;
    }
    public get reseller(): Reseller {
        return this._reseller;
    }
    public set reseller(value: Reseller) {
        this._reseller = value;
    }
    public get product(): Product {
        return this._product;
    }
    public set product(value: Product) {
        this._product = value;
    }
    public get county(): County {
        return this._county;
    }
    public set county(value: County) {
        this._county = value;
    }
    public get state(): State {
        return this._state;
    }
    public set state(value: State) {
        this._state = value;
    }
    public get region(): Region {
        return this._region;
    }
    public set region(value: Region) {
        this._region = value;
    }
    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }

}
