export class Reseller {
    private _resellerCnpj: string;
    private _id: number;
    private _resellerName: string;
    
    public get resellerName(): string {
        return this._resellerName;
    }
    public set resellerName(value: string) {
        this._resellerName = value;
    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
    public get resellerCnpj(): string {
        return this._resellerCnpj;
    }
    public set resellerCnpj(value: string) {
        this._resellerCnpj = value;
    }
}
