package com.indra.recruitment.Challenger.JPA.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.indra.recruitment.Challenger.JPA.model.Region;

@Repository
public interface RegionRepository extends PagingAndSortingRepository<Region, Integer> {
	
	Optional<Region> findByName(String name);
	
}
