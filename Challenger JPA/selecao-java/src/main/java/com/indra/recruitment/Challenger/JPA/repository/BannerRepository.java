package com.indra.recruitment.Challenger.JPA.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.indra.recruitment.Challenger.JPA.model.Banner;

import java.util.Optional;

@Repository
public interface BannerRepository extends PagingAndSortingRepository<Banner, Integer> {

    Optional<Banner> findByName(String name);
}
