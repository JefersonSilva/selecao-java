package com.indra.recruitment.Challenger.JPA.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.indra.recruitment.Challenger.JPA.model.State;

import java.util.Optional;

@Repository
public interface StateRepository extends PagingAndSortingRepository<State, Integer> {

	Optional<State> findByUf(String string);

}
