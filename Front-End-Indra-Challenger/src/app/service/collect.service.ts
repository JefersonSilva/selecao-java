import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class CollectService {

  private collectsUrl: string;
  private fileUrl: string;
  private page: string;

  constructor(private http: HttpClient) {
    this.collectsUrl = 'http://localhost:8080/user/collections';
    this.fileUrl = '/files';
    this.page = '?page='

  }

  public findAll(): Observable<any> {
    return this.http.get<any>(this.collectsUrl);
  }

  findAllWithPage(pageNumber: number) {
    return this.http.get<any>(`${this.collectsUrl}${this.page}${pageNumber}`);
  }

  public findById(id: number): Observable<any> {
    return this.http.get<any>(`${this.collectsUrl}${id}`)
  }

  public upload(file: File): Observable<any> {
    return this.pushFileToStorage(file)
  }

  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    const data: FormData = new FormData();
    data.append('file', file);
    const newRequest = new HttpRequest('POST', `${this.collectsUrl}${this.fileUrl}`, data, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(newRequest);
  }

}
