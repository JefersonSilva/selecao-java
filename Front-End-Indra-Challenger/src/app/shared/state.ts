export class State {
    private _id: number;
    private _uf: string;
    public get uf(): string {
        return this._uf;
    }
    public set uf(value: string) {
        this._uf = value;
    }

    public get id(): number {
        return this._id;
    }
    public set id(value: number) {
        this._id = value;
    }
}
