import { Component, OnInit, PipeTransform, Input } from '@angular/core';
import { Collect } from 'src/app/shared/collect';
import { CollectService } from 'src/app/service/collect.service';

@Component({
  selector: 'app-collect-list',
  templateUrl: './collect-list.component.html',
  styleUrls: ['./collect-list.component.css']
})
export class CollectListComponent implements OnInit {

  @Input()
  private _collects: Collect[];
  private file: File;
  private page: number = 1;


  constructor(private collectService: CollectService) { }

  ngOnInit() {
    this.getCollects();
  }

  public get collects(): Collect[] {
    return this._collects;
  }
  public set collects(value: Collect[]) {
    this._collects = value;
  }

  public getCollects() {
    this.collectService.findAll().subscribe(data => {
      this._collects = data.content;
    })
  }

  public getNextPage() {
    if (this.page < this._collects.length) {
      this.page++;
      this.collectService.findAllWithPage(this.page).subscribe(data => {
        this._collects = data.content;
      })
    }
  }

  public getPreviowsPage() {
    if (this.page != 0) {
      this.page--;
      this.collectService.findAllWithPage(this.page).subscribe(data => {
        this._collects = data.content;
      })
    }
  }

  public getCollect(id: number) {
    this.collectService.findById(id).subscribe(data => {
    });
  }

  public selectFile(files: FileList) {
    this.file = files.item(0);
  }

  public uploadFile() {
    console.log(this.file);
    this.collectService.upload(this.file).subscribe(data => {
      this.getCollects();
    });
  }


}
