package com.indra.recruitment.Challenger.JPA.service;

import java.util.List;

import com.indra.recruitment.Challenger.JPA.model.Collect;
import com.indra.recruitment.Challenger.JPA.repository.BannerRepository;
import com.indra.recruitment.Challenger.JPA.repository.CollectRepository;
import com.indra.recruitment.Challenger.JPA.repository.CountyRepository;
import com.indra.recruitment.Challenger.JPA.repository.ProductRepository;
import com.indra.recruitment.Challenger.JPA.repository.RegionRepository;
import com.indra.recruitment.Challenger.JPA.repository.ResellerRepository;
import com.indra.recruitment.Challenger.JPA.repository.StateRepository;
import com.indra.recruitment.Challenger.JPA.util.file.CollectsReaderUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImportColletionsService {

    @Autowired
    private CollectRepository collectRepository;
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private BannerRepository bannerRepository;
    @Autowired
    private CountyRepository countyRepository;
    @Autowired
    private ResellerRepository resellerRepository;
    @Autowired
    private StateRepository stateRepository;


    // Import Data From Csv File
    public void importCsv(MultipartFile file) {
        this.collectRepository.saveAll(getCollectionsFromCsv(file));
    }

    private List<Collect> getCollectionsFromCsv(MultipartFile file) {
        // String fileAsString = new String(file.getBytes(), StandardCharsets.UTF_8);
        return new CollectsReaderUtil(regionRepository, productRepository, bannerRepository, countyRepository,
                stateRepository, resellerRepository).readCsv(file);
    }
}