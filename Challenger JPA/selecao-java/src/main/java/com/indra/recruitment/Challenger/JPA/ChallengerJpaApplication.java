package com.indra.recruitment.Challenger.JPA;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengerJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengerJpaApplication.class, args);
		System.out.println("ok");
	}

}
