package com.indra.recruitment.Challenger.JPA.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.indra.recruitment.Challenger.JPA.model.Collect;

@Repository
public interface CollectRepository extends PagingAndSortingRepository<Collect, Long> {

	@Query(value = "SELECT AVG(col.purchasePrice) FROM Collect AS col INNER JOIN County AS cou ON col.county.id = cou.id AND cou.name=?1")
	Double findAverageFuelPurchasePriceBasedOnCountyName(String countyName);
	
	@Query(value = "SELECT AVG(col.salePrice) FROM Collect AS col INNER JOIN County AS cou ON col.county.id = cou.id AND cou.name=?1")
	Double findAverageFuelSalePriceBasedOnCountyName(String countyName);
	
	@Query(value = "SELECT col FROM Collect AS col INNER JOIN Region AS reg ON col.region.id = reg.id AND reg.name=?1")
	Page<Collect> findCollectsByRegionName(String regionName, Pageable pageable);
	
	@Query(value="SELECT col FROM Collect AS col INNER JOIN Reseller AS re ON col.reseller.id = re.id AND re.resellerName=?1")
	Page<Collect> findCollectsByResellerName(String resellerName, Pageable pageable);
	
	Page<Collect> findByCollectionDate(Date collectionDate, Pageable pageable);
	
	@Query(value = "SELECT AVG(col.salePrice) FROM Collect AS col INNER JOIN Banner AS ban ON col.county.id = ban.id AND ban.name=?1")
	Double findAverageFuelSalePriceBasedOnBannerName(String bannerName);

	@Query(value = "SELECT AVG(col.purchasePrice) FROM Collect AS col INNER JOIN Banner AS ban ON col.county.id = ban.id AND ban.name=?1")
	Double findAverageFuelPurchasePriceBasedOnBannerName(String bannerName);
}
