package com.indra.recruitment.Challenger.JPA.service;

import com.indra.recruitment.Challenger.JPA.model.User;
import com.indra.recruitment.Challenger.JPA.repository.UserRepository;

import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter @Setter
public class UserService extends GenericService<User, UserRepository> {

}
