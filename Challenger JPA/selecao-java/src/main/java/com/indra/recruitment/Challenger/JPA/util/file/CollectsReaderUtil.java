package com.indra.recruitment.Challenger.JPA.util.file;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.indra.recruitment.Challenger.JPA.model.Banner;
import com.indra.recruitment.Challenger.JPA.model.Collect;
import com.indra.recruitment.Challenger.JPA.model.County;
import com.indra.recruitment.Challenger.JPA.model.Product;
import com.indra.recruitment.Challenger.JPA.model.Region;
import com.indra.recruitment.Challenger.JPA.model.Reseller;
import com.indra.recruitment.Challenger.JPA.model.State;
import com.indra.recruitment.Challenger.JPA.repository.BannerRepository;
import com.indra.recruitment.Challenger.JPA.repository.CountyRepository;
import com.indra.recruitment.Challenger.JPA.repository.ProductRepository;
import com.indra.recruitment.Challenger.JPA.repository.RegionRepository;
import com.indra.recruitment.Challenger.JPA.repository.ResellerRepository;
import com.indra.recruitment.Challenger.JPA.repository.StateRepository;
import com.indra.recruitment.Challenger.JPA.util.DateUtil;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CollectsReaderUtil {

	private final RegionRepository regionRepository;
	private final ProductRepository productRepository;
	private final BannerRepository bannerRepository;
	private final CountyRepository countyRepository;
	private final StateRepository stateRepository;
	private final ResellerRepository resellerRepository;

	private final Logger logger = LoggerFactory.getLogger(CollectsReaderUtil.class);

	/**
	 * Row format: Region_initials - State_initials - County - Reseller -
	 * Reseller_CNPJ - Product - Date_collect - Sale_Price - Purchase_Price -
	 * Measurement_Unit - Banner
	 * 
	 * 
	 */
	public List<Collect> readCsv(MultipartFile multipartFile) {
		this.logger.info("Processing file...");
		List<Collect> collects = new ArrayList<>();
		int invalidRecords = 0;
		Instant start = Instant.now();
		File file = new File(multipartFile.getOriginalFilename());
		try(LineIterator lineIterator = FileUtils.lineIterator(file, "UTF-8")) {
			//skip headers 
			lineIterator.nextLine();
			while (lineIterator.hasNext()) {
				String[] columns = lineIterator.nextLine().split("\\s\\s");
				if (columns.length == 11) {
					collects.add(createCollect(columns));
				} else {
					invalidRecords++;
				}

			}
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			Instant finish = Instant.now();
			this.logger.info("Processing time: " + (Duration.between(start, finish).toMillis() / 1000) + " second(s)");
			this.logger.info("Invalid data registers: " + invalidRecords);
		}
		return collects;
	}

	private Collect createCollect(String[] columns) throws ParseException {
		Collect collect = new Collect();
		// Region
		collect.setRegion(findRegion(trim(columns[0])));
		// State
		collect.setState(findState(trim(columns[1])));
		// County
		collect.setCounty(findCounty(trim(columns[2])));
		// Reseller
		collect.setReseller(findReseller(trim(columns[3]), trim(columns[4])));
		// Product
		collect.setProduct(findProduct(trim(columns[5])));
		// Date
		collect.setCollectionDate(getDate(trim(columns[6])));
		// Purchase price
		collect.setPurchasePrice(getPrice(trim(columns[7])));
		// Sale price
		collect.setSalePrice(getPrice(trim(columns[8])));
		// Unit
		collect.setMeasurementUnit(trim(columns[9]));
		// Banner
		collect.setBanner(findBanner(trim(columns[10])));
		return collect;
	}

	private Reseller findReseller(String name, String cnpj) {
		Optional<Reseller> reseller = this.resellerRepository.findByResellerName(name);
		return (reseller.isPresent()) ? reseller.get() : saveReseller(name, cnpj);
	}

	private Reseller saveReseller(String name, String code) {
		Reseller reseller = new Reseller();
		reseller.setResellerName(name);
		reseller.setInstalationCode(code);
		return this.resellerRepository.save(reseller);
	}

	private State findState(String stateInitial) {
		Optional<State> stateSaved = this.stateRepository.findByUf(stateInitial);
		return (stateSaved.isPresent()) ? stateSaved.get() : saveState(stateInitial);
	}

	private State saveState(String stateInitials) {
		State stateSaved = new State();
		stateSaved.setUf(stateInitials);
		return this.stateRepository.save(stateSaved);
	}

	private Region findRegion(String name) {
		Optional<Region> regionSaved = this.regionRepository.findByName(name);
		return (regionSaved.isPresent()) ? regionSaved.get() : saveRegion(name);
	}

	private Region saveRegion(String name) {
		Region regionSaved = new Region();
		regionSaved.setName(name);
		return this.regionRepository.save(regionSaved);
	}

	private County findCounty(String countyName) {
		Optional<County> countySaved = this.countyRepository.findByName(countyName);
		return (countySaved.isPresent()) ? countySaved.get() : saveCounty(countyName);
	}

	private County saveCounty(String name) {
		County countySaved = new County();
		countySaved.setName(name);
		return this.countyRepository.save(countySaved);
	}

	private Product findProduct(String name) {
		Optional<Product> productSaved = productRepository.findByName(name);
		return (productSaved.isPresent()) ? productSaved.get() : saveProduct(name);
	}

	private Product saveProduct(String name) {
		Product product = new Product();
		product.setName(name);
		return this.productRepository.save(product);
	}

	private Banner findBanner(String name) {
		Optional<Banner> bannerSaved = bannerRepository.findByName(name);
		return (bannerSaved.isPresent()) ? bannerSaved.get() : saveBanner(name);
	}

	private Banner saveBanner(String name) {
		Banner banner = new Banner();
		banner.setName(name);
		return this.bannerRepository.save(banner);
	}

	private Double getPrice(String column) {
		return (column.isEmpty()) ? null : Double.parseDouble(removeInvalidCharacters(column));
	}

	private Date getDate(String column) throws ParseException {
		return (column.isEmpty()) ? null : DateUtil.getLocalDate(removeInvalidCharacters(column));
	}

	private String removeInvalidCharacters(String column) {
		column = column.replaceAll("\"", "");
		return column;
	}

	private String trim(String column){
		return column.trim();
	}
}
