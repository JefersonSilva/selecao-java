import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollectListComponent } from './component/collect-list/collect-list.component';


const routes: Routes = [
  {path:"collects", component: CollectListComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
