package com.indra.recruitment.Challenger.JPA.service;

import com.indra.recruitment.Challenger.JPA.exception.NotFoundException;
import com.indra.recruitment.Challenger.JPA.model.GenericModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class GenericService<M extends GenericModel<Long>,R extends PagingAndSortingRepository<M, Long>> {

    @Autowired
    private R repository;

     // CREATE
     public M create(M model) {
        return this.repository.save(model);
    }

    // READ
    public M findById(long id) {
        return this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Model not found by id: " + id));
    }

    // UPDATE
    public M update(Long id, M model) {
        return this.repository.save(model);

    }

    // DELETE
    public M deleteById(Long id) {
        M model = this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Model not found: " + id));
        this.repository.delete(model);
        return model;
    }

}